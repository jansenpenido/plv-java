import java.util.*;

// Questão 2
public class Execucao 
{
	public static double calcularMedia ( double[] valores )
	{
		double soma = 0;
		
		for( int i = 0 ; i < valores.length ; i++ ) {
			soma += valores[i];
		}
		
		return ( soma / valores.length );
	}
	
	// TODO
	public static double[] acimaDaMedia ( double[] valores )
	{
		double media = calcularMedia( valores );
		int j = 0;		
		
		double[] valoresAcima = new double[valores.length];
		
		for( int i = 0 ; i < valores.length ; i++ ) {
			if( valores[i] > media ) {
				valoresAcima[j] = valores[i];
				j++;
			} 
		}
		
		double[] n = Arrays.copyOfRange(valoresAcima, 0, j); 
		return n;
	}

	
	public static void main ( String[] args )
	{
		double num[] = {7, 9, 7, 8, 2, 3, 10, 9, 6, 3};
		
		System.out.println( "Média: " + calcularMedia(num) );
		
		System.out.println( "Valores acima da média: " + 
							Arrays.toString( acimaDaMedia(num) ) );
	}
}
