// Questão 2
public class Execucao 
{
	public static String formatarNome ( String nome )
	{
		String[] str = nome.split(" ");
		
		String nomeFormatado = str[str.length-1];
		
		nomeFormatado += ", ";
		
		for( int i = 0 ; i < (str.length - 1) ; i++ ) {
			nomeFormatado += str[i].substring(0,1) + ". ";
		}
				
		return nomeFormatado;
	}
	
	public static void main ( String[] args )
	{
		String nomeA = "Sara Jane Smith";
		String nomeB = formatarNome(nomeA);
		
		System.out.println( "Entrada: " + nomeA );
		System.out.println( "Saída: " + nomeB );
	}

}
