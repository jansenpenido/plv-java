
public class Execucao 
{
	public static boolean testarPrimo ( int num )
	{
	    if ( num == 0 ) {       // Como zero por definição não é um nº primo:
	        return false;       // retornar falso.
	    }

	    // Caso seja outro número...
	    for ( int i = 2 ; i <= Math.sqrt(num) ; i++ )  // Dividir de 2 até a raiz do num testado...
	    {
	        if ( num % i == 0 ) {       				// Se alguma divisão não for exata
	            return false;           				// significa que o nº não é primo.
	        }
	    }

	    return true;
	}
	
	public static void main ( String[] args )
	{
		int n = 419;
		
		if( testarPrimo(n) ) {
			System.out.println(n + " é um número primo!");
		} else {
			System.out.println(n + " não é primo...");
		}
	}
}
