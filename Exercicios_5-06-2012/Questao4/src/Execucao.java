// Questão 4
public class Execucao 
{
	public static String espelhar( String a ) 
	{
		StringBuilder b = new StringBuilder();
		
		for( int i = a.length() - 1 ; i > 0 ; i-- ) 
		{
			b.append(a.charAt(i));
		}
		
		return b.toString();
	}
	
	public static void main ( String[] args )
	{
		String str = "Uma frase qualquer";
		
		System.out.println("Frase original: " + str);
		System.out.println("Frase invertida: " + espelhar(str));
	}
}
