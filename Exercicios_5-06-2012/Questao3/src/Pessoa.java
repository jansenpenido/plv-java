
public class Pessoa 
{
	char genero;
	double peso;
	double altura;
	
	public Pessoa () {}
	
	public Pessoa ( char cGenero, double dPeso, double dAltura ) {
		genero = cGenero;
		peso = dPeso;
		altura = dAltura;
	}
	
	public String informarSituacao () 
	{
		double IMC = peso / Math.pow(altura, 2);
		System.out.println("IMC = " + IMC);
		
		if( (genero == 'm' && IMC < 19.1) || (genero == 'h' && IMC < 20.7) ) {
			return "Abaixo do peso";
		}
			
		if( (genero == 'm' && IMC >= 25.8) || (genero == 'h' && IMC >= 26.4) ) {
			return "Acima do peso";
		}
		
		return "Peso ideal";
	}
}
