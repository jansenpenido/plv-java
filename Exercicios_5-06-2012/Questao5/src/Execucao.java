// Questão 5
public class Execucao 
{
	public static double mediaPonderada ( double[] notas, double[] pesos )
	{
		double numerador = 0;
		double denominador = 0;
		
		// Calcular numerador: soma das notas x pesos
		for( int i = 0 ; i < notas.length ; i++ ) {
			numerador += notas[i] * pesos[i];
		}
		
		// Calcular denominador: soma dos pesos
		for( int i = 0 ; i < pesos.length ; i++ ) {
			denominador += pesos[i];
		}
		
		return (numerador / denominador);
	}
	
	public static String informarSituacaoAluno ( double media )
	{
		if ( media >= 6.5 ) {
			return "aprovado";
		}
		
		if ( media < 3.5 ) {
			return "reprovado";
		}
		
		return "recuperação";
	}
	
	
	public static void main ( String[] args )
	{
		double[] notas = {7,9,5};
		double[] pesos = {5,3,2};
		
		//System.out.println("Média: " + mediaPonderada(notas, pesos));
		System.out.println( "O aluno está " + informarSituacaoAluno( mediaPonderada(notas, pesos) ) + "!");
	}
}
