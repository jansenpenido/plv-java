################################################################################
############     Códigos JAVA - Programação e Linguagem Visual      ############
################################################################################

Como importar os programas para a IDE Eclipse:

 1) Acessem o menu "File > Import..."

 2) No campo filtro escrevam “Git”. Deve aparecer a opção "Git" sob a pasta 
 	"Projects from Git", clique Next.

 3) Selecionem “URI” e clique Next.

 4) No campo “URI”, colem o endereço do repositório 
 	("git://gitorious.org/​jansen-project/​plv-java.git"). Os outros campos serão 
 	preenchidos de acordo com o valor deste. Next.

 5) Aguarde um instante, e depois deixe o ramo "master" marcado. Next.

 6) Aí você pode deixar como está ou escolher uma pasta diferente para copiar 
 	os repositório. Next.

 7) Deixe a primeira opção marcada para importar os projetos a seguir. Next.

 8) Marque os projetos que deseja importar deste repositório e clique Finish. 
 	(Atenção: Se existirem quaisquer projetos com nomes iguais, no repositório e 
 	no Eclipse, estes não poderão ser importados!)


Tutorial escrito por:
  Rafael Barbosa Lopes <https://www.gitorious.org/~rb-lopes>

