public class TiposPrimitivos 
{
	public static void main( String args[ ] )
	{
		int a = 4;
		int b = 5;
		
		int c;
		c = a / b;
		System.out.println("c = " + c);
		
		double d;
		d = a / b;							// a divis�o de inteiros resulta em um valor inteiro
		System.out.println("d = " + d);
		
		double e;
		e = ( (double) a / (double) b );	// realiza casting para retornar o valor decimal
		System.out.println("e = " + e);
	}
}
