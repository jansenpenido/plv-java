// Classe de negócio
public class Gato 
{
	// Atributos
	protected String p_sNome;
	protected String p_sRaca;
	protected String p_sSexo;
	protected String p_sCor;
	protected boolean p_fAndando;
	
	public static int numGatos = 5;
	
	public static int obterNumGatos() {
		return (numGatos);
	}
	
	
	// Construtor vazio...
	public Gato () {}
	
	// Construtor
	public Gato( String sNome, String sRaca, String sSexo, String sCor )
	{
		this.p_sNome = sNome;
		this.p_sRaca = sRaca;
		this.p_sSexo = sSexo;
		this.p_sCor = sCor;
		this.p_fAndando = false;
	}
	
	// Métodos
	public void alterarTudo( String sNome, String sRaca, String sSexo, String sCor )
	{
		this.p_sNome = sNome;
		this.p_sRaca = sRaca;
		this.p_sSexo = sSexo;
		this.p_sCor = sCor;
	}
	
	public String obterNome() {
		return (p_sNome);
	}
	
	public void alterarNome( String sNome ) {
		this.p_sNome = sNome;
	}
	
	public String obterRaca() {
		return (p_sRaca);
	}
	
	public void alterarRaca( String sRaca ) {
		this.p_sRaca = sRaca;
	}
	
	public String obterSexo() {
		return (p_sSexo);
	}
	
	public void alterarSexo( String sSexo ) {
		this.p_sSexo = sSexo;
	}
	
	public String obterCor() {
		return (p_sCor);
	}
	
	public void alterarCor( String sCor ) {
		this.p_sCor = sCor;
	}
	
	public boolean estarAndando(){
		return (p_fAndando);
	}
	
	public void andar()	{
		this.p_fAndando = true;
	}
	
	public void parar() {
		this.p_fAndando = false;
	}
	
	//O que mais se aproxima de destrutor em Java
	protected void finalizar() {
		this.p_sNome = null;
		this.p_sRaca = null;
		this.p_sSexo = null;
		this.p_sCor = null;
	}
	
	public void imprimir() 
	{
		System.out.println("\n" + this.p_sNome);
		System.out.println("----------------------");
		System.out.println("Raça: " + this.p_sRaca);
		System.out.println("Sexo: " + this.p_sSexo);
		System.out.println("Cor: " + this.p_sCor);
	}
}
