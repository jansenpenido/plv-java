/// Classe de execução
public class TelaExecucao 
{

	public static void main ( String[] args )
	{
		// Alocação dos animais
		Gato n = new Gato("Niki", "persa", "feminino", "cinza");
		
		Gato f = new Gato();
		f.alterarTudo("Felix", "siamês", "masculino", "preto");
		
		Leao l = new Leao();
		l.alterarTudo("Simba", "normal", "masculino", "bege", "marrom");
		
		// Exibir dados ao usuário
		System.out.println("Animais do Zoológico:");
		
		f.imprimir();
		n.imprimir();
		l.imprimir();
		
		n.alterarTudo("Garfeild", "listrado", "masculino", "laranja");
		
		n.imprimir();
		
		//numGatos = 5;
		System.out.println("\nNº de gatos: " + Gato.obterNumGatos());
		
		// Liberar memória
		l.finalizar();
		f.finalizar();
		n.finalizar();
	}
}
