public class Leao extends Gato
{
	private String p_sJuba;
	
	// Construtor vazio...
	public Leao () {}
	
	// Construtor
	public Leao( String sNome,String sRaca, String sSexo, String sCor, String sJuba ) 
	{
		super( sNome, sRaca, sSexo, sCor );
		this.p_sJuba = sJuba;
	}
	
	// M�todos
	public void alterarTudo (  String sNome, String sRaca, String sSexo, String sCor, String sJuba  ) {
		super.alterarTudo(sNome, sRaca, sSexo, sCor);
		this.p_sJuba = sJuba;
	}
	
	public String obterJuba() {
		return this.p_sJuba;
	}
	
	public void alterarJuba( String sJuba ) {
		this.p_sJuba = sJuba;
	}
	
	public String rugir() {
		return "Ruuuhhhhhhhhhhh";
	}
	
	public void imprimir() 
	{
		super.imprimir();
		System.out.println("Cor da juba: " + this.p_sJuba);
	}
}
