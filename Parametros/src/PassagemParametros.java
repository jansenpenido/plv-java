/* Ilustra como funciona a passagem de par�metros em JAVA.
 * */

public class PassagemParametros {

	public static void main(String[] args) {
		int i = 10;
		int[] dados = { 1, 2, 3 };
		
		alterarDados(i, dados);				// passa o int e o vetor
		
		// Demonstra que o int n�o foi modificado.
		System.out.println("i = " + i);

		// Mostra que os valores dos slots do vetor foram modificados
		for (int j = 0; j < dados.length; j++) {
			System.out.println("dados[" + j + "] = " + dados[j]);
		}
	}

	/* Modifica o valor de um inteiro (passagem por valor) 
	 * e slots de um vetor (passagem por refer�ncia)
	 */
	private static void alterarDados(int i, int[] dados) {
		i = 4;		// Modifica a c�pia local, dentro deste m�todo. 
		
		// Modifica os slots do vetor dados original
		for (int j = 0; j < dados.length; j++) {
			dados[j] = j + 10;
		}
	}
}