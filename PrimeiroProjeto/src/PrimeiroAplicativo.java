public class PrimeiroAplicativo 
{
	/* Classe responsável por imprimir na tela a seguinte String:
	 * "Este é o meu primeiro aplicativo JAVA!"
	 * */
	
	public static void main(String[] args) throws Exception 
	{
		System.out.println("Este é o meu primeiro aplicativo JAVA!");
	}
}
