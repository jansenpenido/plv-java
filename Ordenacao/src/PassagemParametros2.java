public class PassagemParametros2 
{
	private static void imprimirVetor(String nome, int[] vetor) {
		for (int i = 0; i < vetor.length; i++) {
			System.out.println(nome + "[" + i + "] =" + vetor[i]);
		}
	}

	public static void main(String args[]) {
		int[] original = { 4, 3, 2, 1 };
		System.out.println("Vetor original:");
		imprimirVetor("original", original);
		
		int[] ordenado = ordenar(original);
		System.out.println("\nVetor ordenado:");
		imprimirVetor("ordenar", ordenado);
	}

	private static int[] ordenar(int[] original) 
	{
		int k = 5;
		int n = original.length;
		int i, B[] = new int[n], C[] = new int[k];
		
		// 2
		for( i = 0; i < n; i++ ) {
			C[original[i]]++;
			//System.out.println( C[original[i]] );
		}
		
		// 3
		for( i = 1; i < k; i++ ) {
			C[i] += C[i-1];
		}
		
		// 4
		for( i = n-1; i >= 0; i-- ) {
			B[--C[original[i]]] = original[i];
		}
		
		return B;
	}
}
