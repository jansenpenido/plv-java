// Classe Motorista
public class Motorista extends Pessoa 
{
	protected String CNH;
	
	public Motorista(){}
	
	public Motorista(int inIdade, String inTelefone, String inCPF,
			String inCNH) {
		super(inIdade, inTelefone, inCPF);
		CNH = inCNH;
	}
	
	public String obterCNH(){
		return CNH;		
	}
	
	public String apresentar(){
		return "Oi, eu sou um motorista...";
	}
	
	public String identificar() {
		return "Eu sou um motorista com CNH " + CNH;
	}
	
	public void imprimir(){
		System.out.println("Motorista");
		super.imprimir();
		System.out.println("Nº Habilitação: " + CNH);
		System.out.println("Apresentação: \"" + this.apresentar() + "\"\n");
	}
}
