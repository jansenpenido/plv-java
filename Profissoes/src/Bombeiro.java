// Classe Bombeiro
public class Bombeiro extends Pessoa
{
	protected String tempo_servico;
	
	public Bombeiro(){}
	
	public Bombeiro(int inIdade, String inTelefone, String inCPF,
			String inTempo_Servico) {
		super(inIdade, inTelefone, inCPF);
		tempo_servico = inTempo_Servico;
	}
	
	public String obterTempoServico(){
		return tempo_servico;
	}
	
	public String apresentar(){
		return "Oi, eu sou um bombeiro...";
	}
	
	public void imprimir(){
		System.out.println("Bombeiro");
		super.imprimir();
		System.out.println("Tempo de serviço: " + tempo_servico + " anos");
		System.out.println("Apresentação: \"" + this.apresentar() + "\"\n");
	}
}
