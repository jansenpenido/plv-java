// Classe Policial
public class Policial extends Pessoa
{
	protected String ID_Militar;
	
	public Policial(){}
	
	public Policial(int inIdade, String inTelefone, String inCPF,
			String inID_Militar) {
		super(inIdade, inTelefone, inCPF);
		ID_Militar = inID_Militar;
	}
	
	public String obterIdMilitar(){
		return ID_Militar;
	}

	public String apresentar() {
		return "Oi, eu sou um policial...";
	}
	
	public String identificar() {
		return "Eu sou um policial com id " + ID_Militar;
	}
	
	public String classificar() {
		return "oUtRa cOiSa...";
	}
	
	public void imprimir(){
		System.out.println("Policial");
		super.imprimir();
		System.out.println("Identidade militar: " + ID_Militar);
		System.out.println("Apresentação: \"" + this.apresentar() + "\"\n");
	}
}
