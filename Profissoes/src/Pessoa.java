// Classe abstrata Pessoa
public abstract class Pessoa 
{
	protected int idade;
	protected String telefone;
	protected String CPF;
	
	public Pessoa() {}

	public Pessoa(int inIdade, String inTelefone, String inCPF) {
		idade = inIdade;
		telefone = inTelefone;
		CPF = inCPF;
	}

	public int obterIdade() {
		return idade;
	}

	public String obterTelefone() {
		return telefone;
	}

	public String obterCPF() {
		return CPF;
	}
	
	public String identificar () {
		return "Eu sou uma pessoa...";
	}
	
	public String classificar() {
		return "Eu sou um homo sapiens...";
	}
	
	public abstract String apresentar();
	
	public void imprimir() {
		System.out.println("--------------------------");
		System.out.println("Idade: " + idade);
		System.out.println("Telefone: " + telefone);
		System.out.println("CPF: " + CPF);
		System.out.println("Identificação: \"" + this.identificar() + '\"');
		System.out.println("Classificação: \"" + this.classificar() + '\"');
	}
}
