// Classe de Execução
public class TelaProfissoes 
{
	public static void main( String[] args )
	{
		Bombeiro Jose = new Bombeiro(29, "555-0123", "165161326", "8");
		Policial Joao = new Policial(38, "555-9876", "2159123631", "171-24");
		Motorista Clovis = new Motorista(52 , "555-1234", "594921926", "ABC-1587");
		
		Jose.imprimir();
		Joao.imprimir();
		Clovis.imprimir();
	}
}
